﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sp_hw2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(@"https://speed.hetzner.de/100MB.bin", "100MB.bin");
                MessageBox.Show("DownloadComplete");
            }
        }

        private void ButtonCalcClick(object sender, RoutedEventArgs e)
        {
            var currentDomain = AppDomain.CurrentDomain;
            var secondDomain = AppDomain.CreateDomain("Factorial calculation");
            secondDomain.ExecuteAssembly(secondDomain.BaseDirectory + "sp_hw2a.exe");
            AppDomain.Unload(secondDomain);

        }
    }
}
